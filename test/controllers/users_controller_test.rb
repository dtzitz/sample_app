require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:michael)
    @otheruser = users(:archer)
  end
  
  test "should get new" do
    get :new
    assert_response :success
    assert_select "title", "Sign Up | Ruby on Rails Tutorial Sample App"
  end
  
  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_redirected_to login_url
  end
  
  test "should redirect update when not logged in" do
    patch :update, id: @user, user: { name: @user.name, email: @user.email}
    assert_redirected_to login_url
  end
  
  test "shouldn't let users edit each other" do
    log_in_as(@otheruser)
    get :edit, id: @user
    assert_redirected_to root_url
  end
  
  test "shouldn't let users update each other" do
    log_in_as(@otheruser)
    patch :update, id: @user, user: {name: @user.name, email: @user.email}
    assert_redirected_to root_url
  end
  
  test "should redirect to login from index when not logged in" do
    get :index
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end
  test "should redirect destroy when not logged in as admin" do
    log_in_as(@otheruser)
     assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url
  end

end
