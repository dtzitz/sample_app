require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    ActionMailer::Base.deliveries.clear
    
  end
    
    test "invalid signup information" do
      
      assert_no_difference 'User.count' do
        get signup_path
        post users_path, user: {
          name: "",
          email: "user@invalid",
          password: "foo",
          password_confirmation: "bar"
        }
      end
      assert_template 'users/new'
    end
    
    test "valid signup information with account activation" do
      get signup_path
      name = "Dylan Zitzmann"
      email = "dtzitz@live.com"
      password = "password"
      assert_difference 'User.count', 1 do
        post users_path, user: {
          name: name,
          email: email,
          password: password,
          password_confirmation: password
        }
      end
      assert_equal 1, ActionMailer::Base.deliveries.size
      user =  assigns(:user)
      #try to login before activation
      log_in_as(user)
      assert_not is_logged_in?
      #invalid activaion token
      get edit_account_activation_path("invalid token")
      assert_not is_logged_in?
      #valid token but wrong email
      get edit_account_activation_path(user.activation_token, email: "nope")
      assert_not is_logged_in?
      
      #everything is valid and works
      get edit_account_activation_path(user.activation_token, email: user.email)
      assert user.reload.activated?
      follow_redirect!
      assert_template 'users/show'
      assert is_logged_in?
    end
end
