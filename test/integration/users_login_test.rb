require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "login with INVALID info" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { email: "", password: "" }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?  
  end
  
  test "login with VALID info followed by logout" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { email: @user.email, password: "password"}
    assert flash.empty?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
    #simulate user clicking logout in other window/browser
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end
  
  #this doesn't work, don't know why behavor functions correctly in browser
  # test "should login with remembering" do
  #   log_in_as(@user, remember_me: '1')
  #   assert_not_nil cookies['remember_token']

  # end
  
  
  test "should login without remember" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end


end
