class User < ActiveRecord::Base
    before_save {self.email = email.downcase}
    attr_accessor :remember_token, :activation_token, :reset_token
    before_create :create_activation_digest
    
    #author's regex
    #VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    
    #stackoverflow regex
    VALID_EMAIL_REGEX = /[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/i
    
    validates :name, presence: true, length: { maximum: 50 }
    
    validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, length: { maximum: 100 }, uniqueness: true 
    
    has_secure_password
    
    validates :password, length: { minimum: 6 }, allow_blank: true
    
    #creates a low cost hash for testing passwords (like you don't have an extra 2 seconds to spare)
    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
 
    end
    
    #returns a random token for long term authentication
    def User.new_token
        SecureRandom.urlsafe_base64
    end
    
    #sends actiavtion email
    def send_activation_email
        UserMailer.account_activation(self).deliver_now
    end
    
    #remembers the user in the database for persistant sessions
    def remember
        self.remember_token = User.new_token
        update_attribute(:remember_digest, User.digest(remember_token))
    end
    
    #forgets the user when they explicity log out
    def forget
        update_attribute(:remember_digest, nil)
    end
    
    #sets password reset attributes
    def create_reset_digest
        self.reset_token = User.new_token
        update_attribute(:reset_digest, User.digest(reset_token))
        update_attribute(:reset_sent_at, Time.zone.now)
    end
    
    def send_password_reset_email
        UserMailer.password_reset(self).deliver_now
    end
    
    #returns true if the given token matches the user digest
    def authenticated?(attribute, token)
        digest = send("#{attribute}_digest")
        return false if digest.nil?
        BCrypt::Password.new(digest).is_password?(token)
    end
    
    #returns true if a password reset has expired
    def password_reset_expired?
        reset_sent_at < 2.hours.ago
    end
    
    private
    
    
    
    #creates and assigns the activation token and digest
    def create_activation_digest
        self.activation_token = User.new_token
        self.activation_digest = User.digest(activation_token)
    end
    
end
