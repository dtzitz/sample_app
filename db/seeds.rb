# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#dogInLabCoat.jpg appears to be an initalization of the seed variables
User.create!(name: "Example User", email: "example@railstutorial.org", password: "foobar", password_confirmation: "foobar", admin: true, activated:true, activated_at: Time.zone.now)

#faker gem will take it from here
99.times do |i|
    fname = Faker::Name.name
    femail = Faker::Internet.free_email
    fpassword = Faker::Internet.password(6)
    
    User.create!(name: fname, email: femail, password: fpassword, password_confirmation: fpassword, activated:true, activated_at: Time.zone.now)
end
